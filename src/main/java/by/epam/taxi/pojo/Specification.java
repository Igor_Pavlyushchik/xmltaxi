package by.epam.taxi.pojo;

/**
 * This class is used to encapsulate technical specification of the given variety of the taxi.
 * @author Igor Pavlyushchik
 *         Created on March 09, 2015.
 */
public class Specification {
    private double fuelConsumption; 	// liters for 100km
    private int speed; 			// km/h

    public double getFuelConsumption() {
	return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
	this.fuelConsumption = fuelConsumption;
    }

    public int getSpeed() {
	return speed;
    }

    public void setSpeed(int speed) {
	this.speed = speed;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Specification)) return false;

	Specification that = (Specification) o;

	if (Double.compare(that.fuelConsumption, fuelConsumption) != 0) return false;
	if (speed != that.speed) return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result;
	long temp;
	temp = Double.doubleToLongBits(fuelConsumption);
	result = (int) (temp ^ (temp >>> 32));
	result = 31 * result + speed;
	return result;
    }

    @Override
    public String toString() {
	return "fuelConsumption=" + fuelConsumption +
	    ", speed=" + speed;
    }
}
