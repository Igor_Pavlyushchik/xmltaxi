package by.epam.taxi.pojo;

import java.util.Comparator;

/**
 * This class is used to describe common fields and methods for
 * all types of taxis. It also provides nested Comparator class
 * to implement sorting by fuelConsumption and if equal by hashcode.
 * @author Igor Pavlyushchik
 */
public abstract class Taxi {
    private int id;
    private String carMake;
    private String carModel;
    private Specification specification = new Specification();
    private String licensePlateNumber;
    private int price; 			// USD

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getCarMake() {
	return carMake;
    }

    public void setCarMake(String carMake) {
	this.carMake = carMake;
    }

    public String getCarModel() {
	return carModel;
    }

    public void setCarModel(String carModel) {
	this.carModel = carModel;
    }

    public Specification getSpecification() {
	return specification;
    }

    public void setSpecification(Specification specification) {
	this.specification = specification;
    }

    public String getLicensePlateNumber() {
	return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
	this.licensePlateNumber = licensePlateNumber;
    }

    public int getPrice() {
	return price;
    }

    public void setPrice(int price) {
	this.price = price;
    }

    /**
     * This nested implementation of the Comparator is used to sort Taxi instances by
     * fuel consumption.
     */
    public static class FuelComparator implements Comparator<Taxi> {
	public int compare(Taxi one, Taxi two) {
	    int fuelCompare = Double.compare(one.getSpecification().getFuelConsumption(),
			two.getSpecification().getFuelConsumption());
	    if(fuelCompare != 0) {
		return fuelCompare;
	    } else return two.hashCode() - one.hashCode();
	}
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Taxi)) return false;

	Taxi taxi = (Taxi) o;

	if (Double.compare(taxi.getSpecification().getFuelConsumption(), getSpecification().getFuelConsumption()) != 0) return false;
	if (id != taxi.id) return false;
	if (price != taxi.price) return false;
	if (getSpecification().getSpeed() != taxi.getSpecification().getSpeed()) return false;
	if (carMake != null ? !carMake.equals(taxi.carMake) : taxi.carMake != null) return false;
	if (carModel != null ? !carModel.equals(taxi.carModel) : taxi.carModel != null) return false;
	if (licensePlateNumber != null ? !licensePlateNumber.equals(taxi.licensePlateNumber) : taxi.licensePlateNumber != null)
	    return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result;
	long temp;
	result = id;
	result = 31 * result + (carMake != null ? carMake.hashCode() : 0);
	result = 31 * result + (carModel != null ? carModel.hashCode() : 0);
	temp = Double.doubleToLongBits(getSpecification().getFuelConsumption());
	result = 31 * result + (int) (temp ^ (temp >>> 32));
	result = 31 * result + getSpecification().getSpeed();
	result = 31 * result + (licensePlateNumber != null ? licensePlateNumber.hashCode() : 0);
	result = 31 * result + price;
	return result;
    }

    @Override
    public String toString() {
	return "{" +
	    "id=" + id +
	    ", carMake='" + carMake + '\'' +
	    ", carModel='" + carModel + '\'' +
	    ", specification={" + specification +
	    "}, licensePlateNumber='" + licensePlateNumber + '\'' +
	    ", price=" + price +
	    '}';
    }
}
