package by.epam.taxi.pojo;

/**
 * This class is used to create and manipulate an instance of the
 * cargo taxi.
 * @author Igor Pavlyushchik
 */
public class CargoTaxi extends Taxi {
    private int cargoCapacity;

    public int getCargoCapacity() {
	return cargoCapacity;
    }

    public void setCargoCapacity(int cargoCapacity) {
	this.cargoCapacity = cargoCapacity;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof CargoTaxi)) return false;
	if (!super.equals(o)) return false;

	CargoTaxi cargoTaxi = (CargoTaxi) o;

	if (cargoCapacity != cargoTaxi.cargoCapacity) return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result = super.hashCode();
	result = 31 * result + cargoCapacity;
	return result;
    }

    @Override
    public String toString() {
	return "\nCargoTaxi{" +
	    "cargoCapacity=" + cargoCapacity +
	    "} " + super.toString();
    }
}
