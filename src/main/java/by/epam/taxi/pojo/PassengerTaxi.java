package by.epam.taxi.pojo;

/**
 * This class is used to create and manipulate an instance
 * of the passenger taxi.
 * @author Igor Pavlyushchik
 */
public class PassengerTaxi extends Taxi {
    private int passengerCapacity;

    public int getPassengerCapacity() {
	return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
	this.passengerCapacity = passengerCapacity;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof PassengerTaxi)) return false;
	if (!super.equals(o)) return false;

	PassengerTaxi that = (PassengerTaxi) o;

	if (passengerCapacity != that.passengerCapacity) return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result = super.hashCode();
	result = 31 * result + passengerCapacity;
	return result;
    }

    @Override
    public String toString() {
	return "\nPassengerTaxi{" +
	    "passengerCapacity=" + passengerCapacity +
	    "} " + super.toString();
    }
}
