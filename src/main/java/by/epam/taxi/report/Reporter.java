package by.epam.taxi.report;

import by.epam.taxi.pojo.Taxi;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/** This class is designed to print out answers to the task.
 * @author Igor Pavlyushchik
 */
public class Reporter {

    public void printTaxiStation(Set<Taxi> taxiSet, String parserName) {
	System.out.println("Created set of taxis with the help of " + parserName.toUpperCase() + "-parser: ");
	System.out.println(taxiSet);
	System.out.println();
    }

    public void printTaxisValue(int taxisValue) {
	System.out.println("Total taxi station value: " + taxisValue + " USD.");
    }

    public void printFuelSort(TreeSet<Taxi> taxiTreeSet) {
	System.out.println("Set of taxis sorted by fuel consumption: ");
	System.out.println(taxiTreeSet);
	System.out.println();
    }

    public void printSpeedRangeTaxi(Taxi taxi, int speedMinimum, int speedMaximum) {
	if(taxi != null) {
	    System.out.println("The taxi complying speed range demand: " + taxi);
	} else {
	    System.out.println("There is no taxi in the park, which maximum speed is between " +
					speedMinimum + " km/h and " + speedMaximum + " km/h.");
	}
    }
}
