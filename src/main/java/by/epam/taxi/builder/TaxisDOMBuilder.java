package by.epam.taxi.builder;

import by.epam.taxi.exception.TechnicalException;
import by.epam.taxi.pojo.CargoTaxi;
import by.epam.taxi.pojo.PassengerTaxi;
import by.epam.taxi.pojo.Taxi;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author Igor Pavlyushchik
 *         Created on March 07, 2015.
 */
public class TaxisDOMBuilder extends AbstractTaxisBuilder {
    private DocumentBuilder documentBuilder;
    public TaxisDOMBuilder() throws TechnicalException {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	try {
	    documentBuilder = factory.newDocumentBuilder();
	} catch (ParserConfigurationException e) {
	    throw new TechnicalException("Error in parser configuration. " + e);
	}
    }

    /**
     * This class creates {@code Set} of taxis, by parsing xml file, which name is passed as a parameter.
     * @param fileName  the {@code String} name of the file
     * @throws TechnicalException
     */
    @Override
    public void buildSetTaxis(String fileName) throws TechnicalException {
	Document document;
	try {
	    // parsing of the xml-document and tree structure creation.
	    document = documentBuilder.parse(fileName);
	    Element root = document.getDocumentElement();
	    // Receiving the list of the child elements <passengerTaxi> and <cargoTaxi>
	    NodeList taxisList = root.getElementsByTagName("passenger-taxi");
	    for (int i = 0; i < taxisList.getLength(); i++) {
		Element taxiElement = (Element) taxisList.item(i);
		PassengerTaxi taxi = new PassengerTaxi();
		buildPassengerTaxi(taxiElement, taxi);
		taxis.add(taxi);
	    }
	    taxisList = root.getElementsByTagName("cargo-taxi");
	    for (int i = 0; i < taxisList.getLength(); i++) {
		Element taxiElement = (Element) taxisList.item(i);
		CargoTaxi taxi = new CargoTaxi();
		buildCargoTaxi(taxiElement, taxi);
		taxis.add(taxi);
	    }
	} catch (IOException e) {
	    throw new TechnicalException("File error or I/O error. " + e);
	} catch (SAXException e) {
	    throw new TechnicalException("Parsing failure. " + e);
	}
    }

    /**
     * This method assigns parsed values to the corresponding instance of {@code PassengerTaxi}.
     * @param taxiElement {@code Element} being parsed
     * @param taxi  instance of {@code PassengerTaxi being created}
     */
    private void buildPassengerTaxi(Element taxiElement, PassengerTaxi taxi) {
	initializeTaxi(taxiElement, taxi);
	taxi.setPassengerCapacity(Integer.parseInt(getElementTextContent(taxiElement, "passenger-capacity")));
    }

    /**
     * This method assigns parsed values to the corresponding instance of {@code CargoTaxi}.
     * @param taxiElement {@code Element} being parsed
     * @param taxi  instance of {@code CargoTaxi} being created
     */
    private void buildCargoTaxi(Element taxiElement, CargoTaxi taxi) {
	initializeTaxi(taxiElement, taxi);
	taxi.setCargoCapacity(Integer.parseInt(getElementTextContent(taxiElement, "cargo-capacity")));
    }

    // initialize fields common for both types of taxis

    /**
     * This method initializes field of {@code Taxi} subclasses instances, which are common for all taxi varieties.
     * @param taxiElement  {@code Element} being parsed
     * @param taxi instance of {@code Taxi} being initialized.
     */
    private void initializeTaxi(Element taxiElement, Taxi taxi) {
	taxi.setId(Integer.parseInt(taxiElement.getAttribute("id").replace("t","")));
	taxi.setCarMake(getElementTextContent(taxiElement, "car-make"));
	taxi.setCarModel(getElementTextContent(taxiElement, "car-model"));
	Element specificationElement = (Element) taxiElement.getElementsByTagName("specification").item(0);
	taxi.getSpecification().setFuelConsumption(Double.parseDouble(
	    getElementTextContent(specificationElement, "fuel-consumption")));
	taxi.getSpecification().setSpeed(Integer.parseInt(getElementTextContent(specificationElement, "speed")));
	taxi.setLicensePlateNumber(getElementTextContent(taxiElement, "license-plate-number"));
	taxi.setPrice(Integer.parseInt(getElementTextContent(taxiElement, "price")));
    }

    // receiving text content of the tag

    /**
     * This method parses and returns the {@code String} form of the content of the corresponding tag.
     * @param element   {@code Element} being parsed
     * @param elementName   {@code String} representation of the name of the tag being parsed.
     * @return  {@code String} form of the content of the corresponding tag
     */
    private static String getElementTextContent(Element element, String elementName) {
	NodeList nodeList = element.getElementsByTagName(elementName);
	Node node = nodeList.item(0);
	return node.getTextContent();
    }
}
