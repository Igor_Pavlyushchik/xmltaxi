package by.epam.taxi.builder;

import by.epam.taxi.exception.LogicalException;
import by.epam.taxi.exception.TechnicalException;
import by.epam.taxi.pojo.Taxi;

import java.util.HashSet;
import java.util.Set;

/**
 * This is an abstract class for different types of builders used in xml-parsing.
 * @author Igor Pavlyushchik
 *         Created on March 07, 2015.
 */
public abstract class AbstractTaxisBuilder {
    // protected as often addressed from the subclass
    protected Set<Taxi> taxis;

    public AbstractTaxisBuilder() {
	taxis = new HashSet<Taxi>();
    }

    public Set<Taxi> getTaxis() {
	return taxis;
    }

    /**
     * This class defines a abstract method to be implemented by all xml-parsing builders.
     * @param fileName  the {@code String} name of the file
     * @throws TechnicalException
     * @throws LogicalException
     */

    abstract public void buildSetTaxis(String fileName) throws TechnicalException, LogicalException;
}
