package by.epam.taxi.builder;

import by.epam.taxi.exception.TechnicalException;

/**
 * This class implements pattern Factory Method to create an instance of one of the builders for xml-parsing.
 * @author Igor Pavlyushchik
 *         Created on March 07, 2015.
 */
public class TaxiBuilderFactory {
    private enum TypeParser {
	SAX, STAX, DOM
    }

    /**
     * This method creates an instance of one of the subclasses of abstract class {@code AbstractTaxisBuilder}.
     * @param typeParser A string representation of the name for the parser to be created.
     * @return an instance of one of the builders-parsers
     * @throws TechnicalException
     */
    public AbstractTaxisBuilder createTaxiBuilder (String typeParser) throws TechnicalException {
	TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
	switch (type) {
	    case DOM:
		return new TaxisDOMBuilder();
	    case SAX:
		return new TaxisSAXBuilder();
	    case STAX:
		return new TaxisStAXBuilder();
	    default:
		throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
	}
    }
}
