package by.epam.taxi.builder;

import by.epam.taxi.exception.LogicalException;
import by.epam.taxi.exception.TechnicalException;
import by.epam.taxi.pojo.CargoTaxi;
import by.epam.taxi.pojo.PassengerTaxi;
import by.epam.taxi.pojo.Specification;
import by.epam.taxi.pojo.Taxi;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This class performs StAX-xml-parsing.
 * @author Igor Pavlyushchik
 *         Created on March 10, 2015.
 */
public class TaxisStAXBuilder extends AbstractTaxisBuilder {
    private XMLInputFactory inputFactory;

    public TaxisStAXBuilder() {
	inputFactory = XMLInputFactory.newInstance();
    }

    /**
     * This method parses xml file and creates {@code Set} of taxis.
     * @param fileName  the {@code String} name of the file
     * @throws TechnicalException
     * @throws LogicalException
     */
    @Override
    public void buildSetTaxis(String fileName) throws TechnicalException, LogicalException {
	String name;
	XMLStreamReader reader = null;
	try(FileInputStream inputStream = new FileInputStream(new File(fileName))) {
	    try {
		reader = inputFactory.createXMLStreamReader(inputStream);
		//StAX parsing.
		while(reader.hasNext()) {
		    int type = reader.next();
		    if(type == XMLStreamConstants.START_ELEMENT) {
			name = reader.getLocalName();
			TaxiEnum temp = TaxiEnum.valueOf(name.replace("-","_").toUpperCase());
			Taxi taxi = null;
			if(temp == TaxiEnum.PASSENGER_TAXI ) {
			    taxi = new PassengerTaxi();
			} else {
			    if (temp == TaxiEnum.CARGO_TAXI) {
				taxi = new CargoTaxi();
			    }
			}
			if(taxi != null) {
			    buildTaxi(reader, taxi);
			    taxis.add(taxi);
			}
		    }
		}
	    } catch (XMLStreamException e) {
		throw new TechnicalException("Exception creating XMLStreamReader. " + e);
	    }
	} catch (FileNotFoundException e) {
	    throw new TechnicalException("File not found." + e);
	} catch (IOException e) {
	    throw new TechnicalException("I/O Exception. " + e);
	}
    }

    private void buildTaxi(XMLStreamReader reader, Taxi taxi) throws XMLStreamException, LogicalException {
	taxi.setId(Integer.parseInt(reader.getAttributeValue(null, TaxiEnum.ID.getValue()).replace("t", "")));
	String name;
	while (reader.hasNext()) {
	    int type = reader.next();
	    switch (type) {
		case XMLStreamConstants.START_ELEMENT:
		    name = reader.getLocalName();
		    switch (TaxiEnum.valueOf(name.toUpperCase().replace("-", "_"))) {
			case CAR_MAKE:
			    taxi.setCarMake(getXMLText(reader));
			    break;
			case CAR_MODEL:
			    taxi.setCarModel(getXMLText(reader));
			    break;
			case SPECIFICATION:
			    taxi.setSpecification(getXMLSpecification(reader));
			    break;
			case PASSENGER_CAPACITY:
			    ((PassengerTaxi)taxi).setPassengerCapacity(Integer.parseInt(getXMLText(reader)));
			    break;
			case CARGO_CAPACITY:
			    ((CargoTaxi)taxi).setCargoCapacity(Integer.parseInt(getXMLText(reader)));
			    break;
			case LICENSE_PLATE_NUMBER:
			    taxi.setLicensePlateNumber(getXMLText(reader));
			    break;
			case PRICE:
			    taxi.setPrice(Integer.parseInt(getXMLText(reader)));
			    break;
		    }
		    break;
		case XMLStreamConstants.END_ELEMENT:
		    name = reader.getLocalName();
		    TaxiEnum temp = TaxiEnum.valueOf(name.replace("-", "_").toUpperCase());
		    if(temp == TaxiEnum.PASSENGER_TAXI || temp == TaxiEnum.CARGO_TAXI) {
			return;
		    }
		    break;
	    }

	}
	throw new LogicalException("Unknown element in tag PassengerTaxi or CargoTaxi.");
    }

    private Specification getXMLSpecification(XMLStreamReader reader) throws XMLStreamException, LogicalException {
	Specification specification = new Specification();
	int type;
	String name;
	while (reader.hasNext()) {
	    type = reader.next();
	    switch (type) {
		case XMLStreamConstants.START_ELEMENT:
		    name =  reader.getLocalName();
		    switch(TaxiEnum.valueOf(name.replace("-", "_").toUpperCase())) {
			case FUEL_CONSUMPTION:
			    specification.setFuelConsumption(Double.parseDouble(getXMLText(reader)));
			    break;
			case SPEED:
			    specification.setSpeed(Integer.parseInt(getXMLText(reader)));
			    break;
		    }
		    break;
		case XMLStreamConstants.END_ELEMENT:
		    name = reader.getLocalName();
		    if(TaxiEnum.valueOf(name.replace("-", "_").toUpperCase()) == TaxiEnum.SPECIFICATION) {
			return specification;
		    }
		    break;
	    }

	}
	throw new LogicalException("Unknown element in tag Specification.");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
	String text = null;
	if(reader.hasNext()) {
	    reader.next();
	    text = reader.getText();
	}
	return text;
    }
}
