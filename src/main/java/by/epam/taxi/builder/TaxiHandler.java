package by.epam.taxi.builder;

import by.epam.taxi.pojo.CargoTaxi;
import by.epam.taxi.pojo.PassengerTaxi;
import by.epam.taxi.pojo.Taxi;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * This class implements methods responsible for handling corresponding parts of the xml-document and errors,
 * during xml-parsing.
 * @author Igor Pavlyushchik
 *         Created on March 10, 2015.
 */
public class TaxiHandler extends DefaultHandler {
    private Set<Taxi> taxis;
    private Taxi current = null;
    private TaxiEnum currentEnum = null;
    private EnumSet<TaxiEnum> withText;
    public TaxiHandler() {
	taxis = new HashSet<>();
	withText = EnumSet.range(TaxiEnum.CAR_MAKE, TaxiEnum.PASSENGER_CAPACITY);
    }
    public Set<Taxi> getTaxis() {
	return taxis;
    }

    /**
     *  This method creates an instance of the corresponding type of the taxi, depending on the element being parsed.
     *  And assigns it with id value.
     */
    public void startElement(String uri, String localName, String qName, Attributes attrs) {
	if("passenger-taxi".equals(localName) || "cargo-taxi".equals(localName)){
	    if("passenger-taxi".equals(localName)) {
		current = new PassengerTaxi();
	    } else {
		current = new CargoTaxi();
	    }
	    current.setId(Integer.parseInt(attrs.getValue(0).replace("t","")));
	}   else {
	    TaxiEnum temp = TaxiEnum.valueOf(localName.replace("-","_").toUpperCase());
	    if(withText.contains(temp)) {
		currentEnum = temp;
	    }
	}
    }

    /**
     *  This method adds previously initialized instance of the {@code Taxi} to the{@code Set<Taxi>}
     */
    public void endElement(String uri, String localName, String qName) {
	if("passenger-taxi".equals(localName) || "cargo-taxi".equals(localName)) {
	    taxis.add(current);
	}
    }

    /**
     *   This method initializes current instance of the subclass of {@code Taxi} with the currently parsed element
     *   value.
     */
    public void characters(char[] ch, int start, int length) {
	String s = new String(ch, start, length).trim();
	if(currentEnum != null) {
	    switch (currentEnum) {
		case CAR_MAKE:
		    current.setCarMake(s);
		    break;
		case CAR_MODEL:
		    current.setCarModel(s);
		    break;
		case FUEL_CONSUMPTION:
		    current.getSpecification().setFuelConsumption(Double.parseDouble(s));
		    break;
		case SPEED:
		    current.getSpecification().setSpeed(Integer.parseInt(s));
		    break;
		case LICENSE_PLATE_NUMBER:
		    current.setLicensePlateNumber(s);
		    break;
		case PRICE:
		    current.setPrice(Integer.parseInt(s));
		    break;
		case PASSENGER_CAPACITY:
		    ((PassengerTaxi) current).setPassengerCapacity(Integer.parseInt(s));
		    break;
		case CARGO_CAPACITY:
		    ((CargoTaxi) current).setCargoCapacity(Integer.parseInt(s));
		    break;
	    }
	    currentEnum = null;
	}
    }
}
