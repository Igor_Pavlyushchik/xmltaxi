package by.epam.taxi.builder;

import by.epam.taxi.exception.TechnicalException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

/**
 * This class performs SAX-xml-parsing.
 * @author Igor Pavlyushchik
 *         Created on March 10, 2015.
 */
public class TaxisSAXBuilder extends AbstractTaxisBuilder {
    private TaxiHandler taxiHandler;
    private XMLReader reader;

    public TaxisSAXBuilder() throws TechnicalException {
	taxiHandler = new TaxiHandler();
	try {
	    reader = XMLReaderFactory.createXMLReader();
	} catch (SAXException e) {
		throw new TechnicalException("Exception of SAX parser.");
	}
	reader.setContentHandler(taxiHandler);
    }

    /**
     * This class builds {@code Set} of taxis by SAX-parsing relevant {@code fileName} file.
     * @param fileName  the {@code String} name of the file
     * @throws TechnicalException
     */
    @Override
    public void buildSetTaxis(String fileName) throws TechnicalException {
	try {
	    reader.parse(fileName);
	} catch (IOException e) {
	    throw new TechnicalException("I/O Exception while parsing." + e);
	} catch (SAXException e) {
	    throw new TechnicalException("SAX parser Exception." + e);
	}
	taxis = taxiHandler.getTaxis();
    }
}
