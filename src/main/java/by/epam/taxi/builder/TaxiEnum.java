package by.epam.taxi.builder;

/**
 * @author Igor Pavlyushchik
 *         Created on March 10, 2015.
 */
public enum TaxiEnum {
    TAXIS("taxis"),
    ID("id"),
    PASSENGER_TAXI("passenger-taxi"),
    CARGO_TAXI("cargo-taxi"),
    CAR_MAKE("car-make"),
    CAR_MODEL("car-model"),
    FUEL_CONSUMPTION("fuel-consumption"),
    SPEED("speed"),
    LICENSE_PLATE_NUMBER("license-plate-number"),
    PRICE("price"),
    CARGO_CAPACITY("cargo-capacity"),
    PASSENGER_CAPACITY("passenger-capacity"),
    SPECIFICATION("specification");

    private String value;

    private TaxiEnum(String value) {
	this.value = value;
    }

    public String getValue() {
	return value;
    }
}
