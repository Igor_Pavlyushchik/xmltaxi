package by.epam.taxi.exception;

/**
 * This custom exception may be thrown when there is a technical failure in the flow of application, like
 * absence of some resource etc.
 * @author Igor Pavlyushchik
 *         Created on February 25, 2015.
 */
public class TechnicalException extends Exception {
    private static final long serialVersionUID = -2630015922896669190L;

    public TechnicalException() {
    }

    public TechnicalException(String message) {
	super(message);
    }

    public TechnicalException(String message, Throwable cause) {
	super(message, cause);
    }

    public TechnicalException(Throwable cause) {
	super(cause);
    }

    public TechnicalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }
}
