package by.epam.taxi.exception;

/**
 * This custom exception may be thrown, when there is a mistake in business logic.
 * @author Igor Pavlyushchik
 *         Created on March 07, 2015.
 */
public class LogicalException extends Exception {

    private static final long serialVersionUID = 2408228326997823189L;

    public LogicalException() {
    }

    public LogicalException(String message) {
	super(message);
    }

    public LogicalException(String message, Throwable cause) {
	super(message, cause);
    }

    public LogicalException(Throwable cause) {
	super(cause);
    }

    public LogicalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }
}
