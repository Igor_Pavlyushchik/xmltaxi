package by.epam.taxi.service;

import by.epam.taxi.pojo.Taxi;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements service methods performing calculations for the task.
 * @author Igor Pavlyushchik
 */
public class TaxiCalculation implements ITaxiCalculation {
    /**
     * This method calculates the total value of the station's taxis.
     * @param setTaxi  Set containing all the taxis.
     * @return  total value of the taxis.
     */
    public int calculateTaxiStationValue(Set<Taxi> setTaxi) {
	int totalTaxiValue = 0;
	Iterator<Taxi> iteratorTaxi = setTaxi.iterator();
	while(iteratorTaxi.hasNext()) {
	    totalTaxiValue += iteratorTaxi.next().getPrice();
	}
	return totalTaxiValue;
    }

    /**
     * This method searches for the taxi which maximum speed is in the given interval.
     * @param setTaxi  Set of the all taxis
     * @param speedMinimum  Minimum possible speed in the required range
     * @param speedMaximum  Maximum possible speed in the required range
     * @return  a taxi instance complying the speed demand
     */
    public Taxi speedRangeComplying (Set<Taxi> setTaxi, int speedMinimum, int speedMaximum) {
	Taxi taxi = null;
	Iterator<Taxi> iteratorTaxi = setTaxi.iterator();
	while(iteratorTaxi.hasNext()) {
	    taxi = iteratorTaxi.next();
	    if(taxi.getSpecification().getSpeed() >= speedMinimum && taxi.getSpecification().getSpeed() <= speedMaximum) {
		return taxi;
	    }
	    taxi = null;
	}
	return taxi;
    }

    /**
     * This method returns TreeSet of taxis, sorted according to fuel consumption.
     * @param taxiSet  Set of all the taxis
     * @return  TreeSet of the taxis, sorted according to fuel consumption.
     */
    public TreeSet<Taxi> sortByFuel(Set<Taxi> taxiSet) {
	TreeSet<Taxi> taxiTreeSet = new TreeSet<>(new Taxi.FuelComparator());
	taxiTreeSet.addAll(taxiSet);
	return taxiTreeSet;
    }
}
