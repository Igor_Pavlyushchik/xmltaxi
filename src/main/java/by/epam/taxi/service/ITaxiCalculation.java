package by.epam.taxi.service;

import by.epam.taxi.pojo.Taxi;
import java.util.Set;
import java.util.TreeSet;

/**
 * This interface describes functionality for the service
 * methods implementing calculation for the task.
 * @author Igor Pavlyushchik
 */

public interface ITaxiCalculation {

    /**
     * This method calculates the total value of the station's taxis.
     * @param setTaxi  Set containing all the taxis.
     * @return  total value of the taxis.
     */
    int calculateTaxiStationValue(Set<Taxi> setTaxi);

    /**
     * This method searches for the taxi which maximum speed is in the given interval.
     * @param setTaxi  Set of the all taxis
     * @param speedMinimum  Minimum possible speed in the required range
     * @param speedMaximum  Maximum possible speed in the required range
     * @return  a taxi instance complying the speed demand
     */
    Taxi speedRangeComplying(Set<Taxi> setTaxi, int speedMinimum, int speedMaximum);

    /**
     * This method returns TreeSet of taxis, sorted according to fuel consumption.
     * @param taxiSet  Set of all the taxis
     * @return  TreeSet of the taxis, sorted according to fuel consumption.
     */
    TreeSet<Taxi> sortByFuel(Set<Taxi> taxiSet);
}
