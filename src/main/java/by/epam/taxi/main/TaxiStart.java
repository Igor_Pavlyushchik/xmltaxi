package by.epam.taxi.main;

import by.epam.taxi.builder.AbstractTaxisBuilder;
import by.epam.taxi.builder.TaxiBuilderFactory;
import by.epam.taxi.exception.LogicalException;
import by.epam.taxi.exception.TechnicalException;
import by.epam.taxi.pojo.Taxi;
import by.epam.taxi.report.Reporter;
import by.epam.taxi.service.TaxiCalculation;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * This console application is used to create a taxi park station {@code HashSet} from xml file with xsd
 * using three different parsers.  After that several operations with the obtained data are done.
 *
 * @author Igor Pavlyushchik
 *
 */
public class TaxiStart {
    public static final String LOG4J = "/logger/log4j.properties";
    private static final String DOM = "dom";
    private static final String SAX = "sax";
    private static final String STAX = "stax";
    private static final String XML_FILE = "data/taxis.xml";
    static {
	Properties logProperties = new Properties();
	try {
	    logProperties.load(TaxiStart.class.getResourceAsStream(LOG4J));
	    PropertyConfigurator.configure(logProperties);
	} catch (IOException e) {
	    throw new ExceptionInInitializerError("Error trying to initialize logger." + e);
	}
    }
    static Logger logger = Logger.getLogger(TaxiStart.class);

    public static void main(String[] args) {
	logger.info("The application is starting.");

	TaxiCalculation taxiCalculation = new TaxiCalculation();

	Reporter reporter = new Reporter();
	TreeSet<Taxi> taxiTreeSet;
	int taxisValue;
	Taxi taxi;
	// Build set of taxis with the help of DOM-parser.
	TaxiBuilderFactory taxisFactory = new TaxiBuilderFactory();
	AbstractTaxisBuilder builder;
	try {
	    builder = taxisFactory.createTaxiBuilder(DOM);
	} catch (TechnicalException e) {
	    logger.error("Exception creating AbstractTaxisBuilder builder for DOM parsing. " + e);
	    return;
	}
	try {
	    builder.buildSetTaxis(XML_FILE);
	} catch (TechnicalException e) {
	    logger.error("Exception building set of taxis " + e);
	} catch (LogicalException e) {
	    logger.error("LogicalException building set of taxis with DOM parser. " + e);
	}

	Set<Taxi> taxiSet = builder.getTaxis();
	logger.info("Taxi station created with DOM-parser: " + taxiSet +"\n");
	reporter.printTaxiStation(taxiSet, DOM);

	// Build set of taxis with the help of SAX-parser.
	try {
	    builder = taxisFactory.createTaxiBuilder(SAX);
	} catch (TechnicalException e) {
	    logger.error("Exception creating AbstractTaxisBuilder for SAX-parsing. " + e);
	    return;
	}
	try {
	    builder.buildSetTaxis(XML_FILE);
	} catch (TechnicalException e) {
	    logger.error("Exception building set of taxis " + e);
	} catch (LogicalException e) {
	    logger.error("LogicalException building set of taxis with SAX parser. " + e);
	}

	taxiSet = builder.getTaxis();
	logger.info("Taxi station created with the help of SAX-parser: " + taxiSet + "\n");
	reporter.printTaxiStation(taxiSet, SAX);

	// Build set of taxis with the help of StAX-parser.
	try {
	    builder = taxisFactory.createTaxiBuilder(STAX);
	} catch (TechnicalException e) {
	    logger.error("Exception creating AbstractTaxisBuilder for StAX-parsing. " + e);
	    return;
	}
	try {
	    builder.buildSetTaxis(XML_FILE);
	} catch (TechnicalException e) {
	    logger.error("Exception building set of taxis " + e);
	} catch (LogicalException e) {
	    logger.error("LogicalException building set of taxis with StAX parser. " + e);
	}

	taxiSet = builder.getTaxis();
	logger.info("Taxi station created with the help of StAX-parser: " + taxiSet + "\n");
	reporter.printTaxiStation(taxiSet, STAX);

	// Calculate taxi station total vehicle value:
	taxisValue = taxiCalculation.calculateTaxiStationValue(taxiSet);
	logger.info("Taxi station value: " + taxisValue);
	reporter.printTaxisValue(taxisValue);

	// Sort taxis by fuel consumption:
	taxiTreeSet = taxiCalculation.sortByFuel(taxiSet);
	logger.info("Taxi station vehicles sorted by fuel consumption: " + taxiTreeSet +"\n");
	reporter.printFuelSort(taxiTreeSet);

	// Show normal and null variants of the search for the taxi with the speed range interval.
	taxi = taxiCalculation.speedRangeComplying(taxiSet, 190, 250);
	logger.info("Taxi complying speed range: " + taxi);
	reporter.printSpeedRangeTaxi(taxi, 190, 250);
	taxi = taxiCalculation.speedRangeComplying(taxiSet, 60, 120);
	logger.info("Taxi complying speed range: " + taxi);
	reporter.printSpeedRangeTaxi(taxi, 60, 120);

	logger.info("The application is finishing.");
    }
}
